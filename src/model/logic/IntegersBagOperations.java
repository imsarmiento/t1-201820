package model.logic;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations{



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}


	public int getMin (IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value)
					min = value;
			}
		}
		return min;
	}
	
	public double varianza(IntegersBag bag){
		double prom = computeMean(bag);
		double var = 0.0;
		double sum = 0;
		int lenght = 0;
		int value = 0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				sum =  ((value - prom )*( value - prom ));
				lenght++;
			}
			if(lenght > 0) var = (Double) sum /(lenght - 1);
		}
		return var;
	}
	
	public double desviacionEstandar(IntegersBag bag){
		double dv = Math.sqrt(varianza(bag));
		return dv;
	}

}
